#!/bin/bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch Polybar, using default config location ~/.config/polybar/config
#polybar primary-bar 2>&1 | tee -a /tmp/polybar.log & disown

# Wait until all processes are killed
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch polybar
echo "---" | tee -a /tmp/polybar1.log /tmp/polybar2.log
polybar main 2>&1 | tee -a /tmp/polybar1.log & disown
polybar external 2>&1 | tee -a /tmp/polybar2.log & disown

echo "Polybar launched..."
