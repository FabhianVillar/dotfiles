#!/usr/bin/env bash

function run {
  if ! pgrep -f "$1" ;
  then
    $@&
  fi
}

xset s off -dpms
setxkbmap latam,es

#run optimus-manager-qt
#run picom
run nm-applet
run pasystray
#run variety
#run megasync
run dropbox
#run keepassxc
run telegram-desktop
run firefox web.whatsapp.com
