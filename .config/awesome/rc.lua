-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
-- pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

-- Theme handling library
local beautiful = require("beautiful")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

-- Notification library
-- local naughty = require("naughty")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    --naughty.notify({ preset = naughty.config.presets.critical,
    --                 title = "Oops, there were errors during startup!",
    --                 text = awesome.startup_errors })
    awful.spawn.with_shell("notify-send --urgency=critical 'Oops, there were errors during startup!' '"..awesome.startup_errors.."'")
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        awful.spawn.with_shell("notify-send --urgency=critical 'Oops, an error happened!' '"..tostring(err).."'") 
        --naughty.notify({ preset = naughty.config.presets.critical,
        --                 title = "Oops, an error happened!",
        --                 text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- Autorun shell script
awful.spawn.with_shell("~/.config/awesome/autorun.sh")

-- {{{ Variable definition
-- Themes define colours, icons, font and wallpapers.
beautiful.init(gears.filesystem.get_configuration_dir() .. "/themes/default/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "kitty"
editor = "nvim"

editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

require("widgets")

require("mappings")

require("rules")

require('signals')
