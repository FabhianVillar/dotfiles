
local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")
local menubar = require("menubar")
local wibox = require("wibox")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi



require("awful.autofocus")

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({
  image = gears.surface(beautiful.logo),
  menu = mymainmenu
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock("%H:%M")

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[2])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons,
        spacing = dpi(10),
        layout = wibox.layout.fixed.vertical,
        widget_template = {
        {
            {
                {
                  -- Text widget
                    {
                        {
                            id     = "index_role",
                            align = "center",
                            valign = "center",
                            font = "sans 12",
                            widget = wibox.widget.textbox,
                        },
                        margins = 0,
                        widget  = wibox.container.margin,
                    },
                    widget = wibox.container.background,
                },
                layout = wibox.layout.fixed.horizontal,
            },
            left  = dpi(20),
            right = dpi(20),
            top = dpi(10),
            bottom = dpi(10),
            widget = wibox.container.margin
        },
        id     = "background_role",
        widget = wibox.container.background,
        -- Add support for hover colors and an index label
        -- TODO: Fix transition between hover to normal background
        create_callback = function(self, c3, index, objects) --luacheck: no unused args
            self:get_children_by_id("index_role")[1].markup = "<b> "..c3.index.." </b>"
            self:connect_signal("mouse::enter", function()
                if self.bg ~= "#ff0000" then
                    self.backup     = self.bg
                    self.has_backup = true
                end
                self.bg = "#ff0000"
            end)
            self:connect_signal("mouse::leave", function()
                --[[ if self.has_backup then self.bg = self.backup end ]]
                
                if objects[index].activate then
                    self.bg = '#0E70A1'
                else
                    self.bg = self.backup
                end
            end)
        end,
        update_callback = function(self, c3, index, objects) --luacheck: no unused args
            self:get_children_by_id("index_role")[1].markup = "<b> "..c3.index.." </b>"
        end,
      },
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons
    }

    -- Create the top panel
    s.top_panel = awful.wibar({
      position = "top",
      screen = s,
      width = s.geometry.width * 0.9,
      height = dpi(45),
      shape = function(cr, width, height)
         gears.shape.rounded_rect(cr, width, height, 30)
      end
    })

    -- Add widgets to the wibox
    s.top_panel:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            wibox.widget.systray(),
            mytextclock,
            s.mylayoutbox,
        },
    }

    -- Create the left panel
    s.left_panel = awful.wibar({
      screen = s,
      position = "left",
      width = dpi(60),
      height = s.geometry.height * 0.7,
      shape = function(cr, width, height)
         gears.shape.rounded_rect(cr, width, height, 30)
      end
    })

    -- Add widget to the left panel
    s.left_panel:setup {
      expand = "none",
      layout = wibox.layout.align.vertical,
      nil,
      {
        layout = wibox.layout.fixed.vertical,
        s.mytaglist
      }
    }

end)
-- }}}

