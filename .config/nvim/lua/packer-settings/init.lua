return require('packer').startup(function() 

-- Packer can manage itself
  use 'wbthomason/packer.nvim'

    -- Post-install/update hook with neovim command
  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'} 

  use 'sainnhe/sonokai'

  use 'feline-nvim/feline.nvim'

  use 'kyazdani42/nvim-web-devicons'

  use {
  "nvim-neo-tree/neo-tree.nvim",
    branch = "v2.x",
    requires = { 
      "nvim-lua/plenary.nvim",
      "kyazdani42/nvim-web-devicons", -- not strictly required, but recommended
      "MunifTanjim/nui.nvim",
    }
  }

  use {
    'lewis6991/gitsigns.nvim',
    tag = 'release' -- To use the latest release
  }

  use {
    'm-demare/hlargs.nvim',
    requires = { 'nvim-treesitter/nvim-treesitter' }
  }

  use "lukas-reineke/lsp-format.nvim"
  use "tpope/vim-fugitive"
  use "neoclide/coc.nvim"

end)
