vim.o.compatible    = false

-- Identation Settings

vim.o.autoindent    = true
vim.o.expandtab     = true
vim.o.shiftwidth    = 4
vim.o.tabstop       = 4

--------------------------

-- UI Settings

vim.o.termguicolors                 = true
vim.g.sonokai_style                 = 'andromeda'
vim.g.sonokai_better_performance    = 1

vim.cmd('colorscheme sonokai')

require('feline').setup()

--------------------------

-- Other stuff 

vim.o.relativenumber = true

---------------------------
