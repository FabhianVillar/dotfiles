if status is-interactive
    # Commands to run in interactive sessions can go here
    set -g theme_display_user yes
    set -g theme_display_sudo_user yes
    set -g theme_show_exit_status yes
    set -g theme_newline_cursor yes
    set -g theme_date_format "+%a %H:%M"
    set -g theme_display_hostname yes
    set -g theme_color_scheme nord
    set -g theme_powerline_fonts no
    set -g theme_nerd_fonts yes

    oh-my-posh init fish | source

    # ---- Enviroment Variables ---------- 

    # System Variables
    
    set -gx EDITOR nvim
    set -gx TERMINAL kitty
    set -U SXHKD_SHELL "/usr/bin/sh"
    set -gx PLEASE doas

    # Commands as variables

    set -gx DOTFILES "git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"

    # ------------------------------------
    
    # -------- Paths --------

    set -gx PROYECTOS /home/fabhian/Documentos/proyectos
    set -gx COSAS /mnt/Cosas
    set -gx SCRIPTS /home/fabhian/scripts
    # PATH extracted from Artix system
    set -gx PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:$HOME/.local/bin:$HOME/bin:$HOME/scripts
    set -gx AWESOMEWM_CONFIG ~/.config/awesome/
    set -gx NVIM_CONFIG ~/.config/nvim/
    set -gx FISH_CONFIG ~/.config/fish/

    # Gentoo paths

    set -gx PERSONAL_REPO /var/db/repos/arkantos-repo

    # ------------------------

    # -------- Aliases ---------

    alias awesome-set-dual-screen "xrandr --auto --output HDMI-1-0 --mode 1920x1080 --right-of eDP-1 --primary"

    # Configuration files/folders

    #alias config-fish "$EDITOR ~/.config/fish/config.fish"
    #alias config-editor "$EDITOR ~/.config/nvim/lua/init.lua"
    #alias config-editor-plugins "$EDITOR ~/.config/nvim/lua/packer-config/init.lua"
    #alias config-awesome-autostart "$EDITOR ~/.config/awesome/autorun.sh"
    #alias config-awesome "cd $AWESOMEWM_CONFIG && $EDITOR"
    #alias config-kitty "$EDITOR ~/.config/kitty/kitty.conf"
    #alias config-bspwm "$EDITOR ~/.config/bspwm/bspwmrc"
    #alias config-bspwm-hotkeys "$EDITOR ~/.config/sxhkd/sxhkdrc"
    #alias config-polybar "$EDITOR ~/.config/polybar/config.ini"
    #alias config-portage "$PLEASE $EDITOR /etc/portage/make.conf"

    # System maintenance/apps commands

    # Gentoo commands
    alias update-system-changed-use "$PLEASE emerge --verbose --update --deep --changed-use @world"
    #alias add-package "$PLEASE emerge"
    alias clean-dependencies "$PLEASE emerge --depclean"
    alias update-system-new-use "$PLEASE emerge --verbose --update --deep --newuse @world"
    #alias add-use-flags "$PLEASE euse"
    alias edit-kernel "$PLEASE genkernel --menuconfig all"

    # Universal commands

    alias update-grub-config "$PLEASE grub-mkconfig -o /boot/grub/grub.cfg"


    # Utilities commands
    alias ls "exa --icons --octal-permissions --long --tree --all --level 1"
    alias check-battery "acpi -i /sys/class/power_supply/BAT1/"
    alias fix-controller "$PLEASE python $SCRIPTS/fixcontroller.py"
    alias office2john "python ~/.scripts/office2john.py"
    alias dotfiles "$DOTFILES"
    alias dotfiles-push "$DOTFILES push -uf origin main"
    alias shutdown "$PLEASE shutdown -h"
    alias please "$PLEASE"

    # -----------------------------

    # ------- Functions -----------

    function edit-config
        switch $argv
            case bash
                $EDITOR ~/.bashrc
            case zsh
                $EDITOR ~/.zshrc
            case fish
                $EDITOR ~/.config/fish/config.fish
            case nvim
                cd ~/.config/nvim
                ranger
            case awesome
                cd ~/.config/awesome
                ranger
            case awesome-autostart
                $EDITOR ~/.config/awesome/autorun.sh
            case kitty
                $EDITOR ~/.config/kitty/kitty.conf
            case mpv
                $EDITOR ~/.config/mpv/mpv.conf
            case paru
                please $EDITOR /etc/paru.conf
            case '*'
                echo "No program or config found. Give a program/config keyword to look up the config file/folder."
        end
    end

    # -----------------------------
end
