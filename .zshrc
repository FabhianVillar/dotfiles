# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi


autoload -Uz compinit promptinit
compinit
promptinit

# This will set the default prompt to the walters theme
# prompt walters

# Block code for nnn, makes cd on quit

if [ -f /usr/share/nnn/quitcd/quitcd.bash_zsh ]; then
    source /usr/share/nnn/quitcd/quitcd.bash_zsh
fi

# --------- Enviroment Variables ---------

# System Variables
export EDITOR=nvim
export TERMINAL=kitty
export NNN_OPTS="eEHrUx"

# --------- Aliases -----------

# Global aliases
alias -g please="doas"
alias -g ls="exa --icons --octal-permissions --long --tree --all"

# Functions
edit-config() {
    case $1 in

        zsh)
            $EDITOR ~/.zshrc
            ;;
        fish)
            $EDITOR ~/.config/fish/config.fish
            ;;
        nvim)
            cd ~/.config/nvim
            n
            ;;
        awesomewm | awesome)
            cd ~/.config/awesome
            n
            ;;
        autostart)
            $EDITOR ~/.config/awesome/autorun.sh
            ;;
        kitty)
            $EDITOR ~/.config/kitty/kitty.conf
            ;;
        *)
            echo "No program or config found. Give a program/config keyword to look up the config file/folder."
            ;;

    esac
}

alias add-package="paru -S"
alias update-system="paru -Syu"
alias remove-package="paru -R"
alias clean-dependencies="paru --clean"
alias add-package-skip-verify='paru -S --mflags "--skipchecksums --skippgpcheck"'
alias fix-controller="please python ~/.scripts/fixcontroller.py"
alias awesome-set-dual-screen="xrandr --auto --output HDMI-1-0 --mode 1920x1080 --right-of eDP1 --primary"
alias dotfiles="git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
alias config-zsh="$EDITOR ~/.zshrc"


# --------- Plugins ---------

source ~/.zplug/init.zsh

zplug 'zplug/zplug', hook-build:'zplug --self-manage'
zplug zsh-users/zsh-syntax-highlighting
zplug zsh-users/zsh-completions
zplug zsh-users/zsh-autosuggestions
zplug marlonrichert/zsh-autocomplete

zplug romkatv/powerlevel10k, as:theme, depth:1

zplug load 

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
